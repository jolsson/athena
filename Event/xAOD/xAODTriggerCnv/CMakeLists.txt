# $Id: CMakeLists.txt 782320 2016-11-04 10:28:41Z krasznaa $
################################################################################
# Package: xAODTriggerCnv
################################################################################

# Declare the package name:
atlas_subdir( xAODTriggerCnv )

# Extra dependencies, based on the build environment:
if( XAOD_STANDALONE )
   set( extra_deps PRIVATE Control/xAODRootAccess )
elseif( XAOD_ANALYSIS )
   set( extra_deps GaudiKernel Control/AthenaKernel )
else()
   set( extra_deps GaudiKernel
      PRIVATE
      Control/AthenaBaseComps
      Control/AthenaKernel
      Control/StoreGate
      Event/EventInfo
      PhysicsAnalysis/AnalysisTrigger/AnalysisTriggerEvent
      Trigger/TrigAnalysis/TrigAnalysisInterfaces
      Trigger/TrigConfiguration/TrigConfHLTData
      Trigger/TrigConfiguration/TrigConfInterfaces
      Trigger/TrigEvent/TrigDecisionEvent
      Trigger/TrigEvent/TrigDecisionInterface
      Trigger/TrigEvent/TrigSteeringEvent )
endif()

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Control/AthToolSupport/AsgTools
   Event/xAOD/xAODTrigger
   ${extra_deps} )

# Build the package differently in standalone and Athena modes:
if( XAOD_STANDALONE )

   # The main library is an installed one in this case:
   atlas_add_library( xAODTriggerCnv
      xAODTriggerCnv/*.h Root/*.cxx
      PUBLIC_HEADERS xAODTriggerCnv
      LINK_LIBRARIES AsgTools xAODTrigger )

   # Plus we also build a dictionary:
   atlas_add_dictionary( xAODTriggerCnvDict
      xAODTriggerCnv/xAODTriggerCnvDict.h
      xAODTriggerCnv/selection.xml
      LINK_LIBRARIES xAODTriggerCnv )

   # The test(s) need(s) ROOT:
   find_package( ROOT COMPONENTS Core Tree RIO )

   # Test(s) in standalone mode:
   atlas_add_test( ut_xaodtriggercnv_triggermenumetadatatool_test
      SOURCES test/ut_xaodtriggercnv_triggermenumetadatatool_test.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} xAODRootAccess xAODTrigger
      xAODTriggerCnv )

elseif( XAOD_ANALYSIS )

   # The main library is a component one in this case:
   atlas_add_component( xAODTriggerCnv
      xAODTriggerCnv/*.h Root/*.cxx src/components/*.cxx
      LINK_LIBRARIES AsgTools xAODTrigger GaudiKernel )

else()

   # The main library is a component one in this case:
   atlas_add_component( xAODTriggerCnv
      xAODTriggerCnv/*.h src/*.cxx Root/*.cxx src/components/*.cxx
      LINK_LIBRARIES AsgTools xAODTrigger GaudiKernel AthenaBaseComps
      AthenaKernel StoreGateLib SGtests EventInfo AnalysisTriggerEvent
      TrigConfHLTData TrigDecisionEvent TrigSteeringEvent )

endif()

# Install files from the package:
atlas_install_headers( xAODTriggerCnv )
atlas_install_python_modules( python/*.py )
atlas_install_joboptions( share/*.py )
