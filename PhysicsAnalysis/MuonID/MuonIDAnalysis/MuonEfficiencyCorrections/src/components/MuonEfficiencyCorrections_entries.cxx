// official CP tools
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"
// MCP tester macros
#include "../MuonEfficiencyCorrections_TestAlg.h"
#include "../TestTrigSF.h"
#include "../MuonTriggerSF_TestAlg.h"

DECLARE_COMPONENT( CP::MuonEfficiencyScaleFactors )

DECLARE_COMPONENT( CP::MuonTriggerScaleFactors )

DECLARE_COMPONENT( CP::MuonEfficiencyCorrections_TestAlg )

DECLARE_COMPONENT( Trig::TestTrigSF )

DECLARE_COMPONENT( Trig::MuonTriggerSF_TestAlg )
